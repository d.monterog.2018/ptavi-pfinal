#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import json
import hashlib
import time
import socket
from uaclient import log
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import secrets


class SmallSMILHandler(ContentHandler):
    """Content Handler."""

    def __init__(self):
        """Inicializamos."""
        self.lista = {}  # Creamos la lista

        self.valores = {'server': ['name', 'ip', 'puerto'],
                        'database': ['path', 'passwdpath'],
                        'log': ['path']}

    def startElement(self, name, atributos):
        """Lee las etiquetas del xml con sus valores."""
        if name in self.valores:
            for att in self.valores[name]:
                self.lista[name + ' ' + att] = atributos.get(att, '')

    def get_tags(self):
        """Devuelve el diccionario con los atributos."""
        return self.lista  # Nos devuelve la lista


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccionario = {}
    diccionariopass = {}
    nonce = {}

    def register2json(self):
        """Creacion del archivo json."""
        with open(localdatabase, 'w') as jsonfile:  # Abrimos en modo escritura
            json.dump(self.diccionario, jsonfile, indent=3)

    def json2registered(self):
        """Leo el archivo json si existe."""
        try:  # Intentamos buscar el archivo local, si esta lo leemos
            with open(localpassword, 'r') as jsonfile:
                self.diccionariopass = json.load(jsonfile)
            with open(localdatabase, 'r') as jsonfile:
                self.diccionario = json.load(jsonfile)
        except FileNotFoundError:  # Si no con el register2json creamos carpeta
            print("Carga de base de datos de contraseñas realizada.")

    def response(self, nonce, pssword):
        """Funcion para creacion de hash."""
        digest = hashlib.sha256()
        digest.update(bytes(nonce + pssword, 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def usuario_caducado(self):
        """Funcion para borrar usuarios caducados."""
        horacentral = time.time() + 3600

        for usuario in self.diccionario.copy():
            fechaexp = self.diccionario[usuario]['Fecha Registro'] \
                       + self.diccionario[usuario]['Expires']
            if horacentral >= fechaexp:
                del self.diccionario[usuario]  # Borramos el usuario

    def handle(self):
        """
        handle method of the server class.

        (all requests will be handled by this method)
        """
        self.json2registered()
        self.usuario_caducado()
        line = self.rfile.read()
        mensaje = line.decode('utf-8')
        ip = self.client_address[0]
        puerto = self.client_address[1]
        log("Recived from: " + ip + ":" + str(puerto) + ": " + mensaje, LOGS)
        registro = mensaje.split(" ")[0]
        sip = mensaje.split(" ")[1].split(":")[0]

        if sip != 'sip':
            respuesta = (b"SIP/2.0 400 Bad Request\r\n\r\n")
            self.wfile.write(respuesta)

        elif registro == 'INVITE':
            userorigin = mensaje.split("\r\n")[5].split("=")[1].split(" ")[0]
            userdestino = mensaje.split(" ")[1].split(":")[1]

            if userorigin and userdestino in self.diccionario:
                IPdestino = self.diccionario[userdestino]['IP']
                Puertodestino = self.diccionario[userdestino]['Puerto']
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IPdestino, Puertodestino))
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
                    log("Sent to " + IPdestino + ":"
                        + str(Puertodestino)
                        + ": " + mensaje, LOGS)
                    data = my_socket.recv(1024)
                    log("Recived from: " + ip + ":"
                        + str(puerto) + ": "
                        + data.decode('utf-8'), LOGS)
                self.wfile.write(data)
                log("Sent to " + ip + ":"
                    + str(puerto) + ": "
                    + data.decode('utf-8'), LOGS)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')
                log("Sent to " + ip + ":" + str(puerto)
                    + ": SIP/2.0 404 User not Found", LOGS)

        elif registro == 'BYE':
            userdestino = mensaje.split(" ")[1].split(":")[1]

            if userdestino in self.diccionario:
                IPdestino = self.diccionario[userdestino]['IP']
                Puertodestino = self.diccionario[userdestino]['Puerto']
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) \
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IPdestino, Puertodestino))
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
                    log("Sent to " + IPdestino + ":"
                        + str(Puertodestino)
                        + ": " + mensaje, LOGS)
                    data = my_socket.recv(1024)
                    log("Recived from: " + ip + ":"
                        + str(puerto) + ": "
                        + data.decode('utf-8'), LOGS)
                self.wfile.write(data)
                log("Sent to " + ip + ":" + str(puerto)
                    + ": " + data.decode('utf-8'), LOGS)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')
                log("Sent to " + ip + ":" + str(puerto)
                    + ": SIP/2.0 404 User not Found", LOGS)
        elif registro == 'ACK':
            userdestino = mensaje.split(" ")[1].split(":")[1]

            if userdestino in self.diccionario:
                IPdestino = self.diccionario[userdestino]['IP']
                Puertodestino = self.diccionario[userdestino]['Puerto']
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM)\
                        as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET,
                                         socket.SO_REUSEADDR, 1)
                    my_socket.connect((IPdestino, Puertodestino))
                    my_socket.send(bytes(mensaje, 'utf-8') + b'\r\n')
                    log("Sent to " + IPdestino + ":"
                        + str(Puertodestino) + ": "
                        + mensaje, LOGS)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')
                log("Sent to " + ip + ":" + str(puerto)
                    + ": SIP/2.0 404 User not Found", LOGS)

        elif registro not in ['INVITE', 'BYE', 'ACK', 'REGISTER']:
            respuesta = (b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
            self.wfile.write(respuesta)
            log("Sent to " + ip + ":" + str(puerto)
                + ": " + respuesta.decode('utf-8'), LOGS)

        elif registro == 'REGISTER':  # Comprobamos si cinciden
            usuario = mensaje.split(" ")[1].split(":")[1]
            expira = mensaje.split("\r\n")[1].split(":")[1]
            sumadetiempo = int(expira)
            horacentral = time.time() + 3600
            puerto = mensaje.split(" ")[1].split(":")[2]

            if usuario in self.diccionario:
                expira = mensaje.split("\r\n")[1].split(":")[1]
                if int(expira) == 0:  # Si pones 0, se borra
                    print("Se ha eliminado el usuario: " + usuario)
                    del self.diccionario[usuario]
                    respuesta = "SIP/2.0 200 OK\r\n\r\n"
                    self.wfile.write(bytes(respuesta, 'utf-8'))
                    log("Sent to " + ip + ":" + str(puerto)
                        + ": " + respuesta, LOGS)
                elif int(expira) != 0:
                    sumadetiempo = int(expira)
                    horacentral = time.time() + 3600
                    puerto = mensaje.split(" ")[1].split(":")[2]
                    self.diccionario[usuario]['Fecha Registro'] = horacentral
                    self.diccionario[usuario]['Expires'] = sumadetiempo
                    self.diccionario[usuario]['Puerto'] = int(puerto)
                    self.diccionario[usuario]['IP'] = ip
                    respuesta = "SIP/2.0 200 OK\r\n\r\n"
                    self.wfile.write(bytes(respuesta, 'utf-8'))
                    log("Sent to " + ip + ":" + str(puerto)
                        + ": " + respuesta, LOGS)
            else:
                if 'Authorization: Digest response' in mensaje:
                    respuestauser = mensaje.split('=')[1].split("\"")[1]
                    response = self.response(self.nonce[usuario],
                                             self.diccionariopass[usuario])
                    if secrets.compare_digest(respuestauser, response):
                        self.diccionario[usuario] = {'IP': ip,
                                                     'Puerto': int(puerto),
                                                     'Fecha Registro':
                                                         horacentral,
                                                     'Expires': sumadetiempo}
                        respuesta = "SIP/2.0 200 OK\r\n\r\n"
                        print("Se ha añadido " + usuario
                              + " a la base de usuarios registrados.")
                        self.wfile.write(bytes(respuesta, 'utf-8'))
                        log("Sent to " + ip + ":"
                            + str(puerto)
                            + ": " + respuesta, LOGS)
                        del self.nonce[usuario]
                    else:
                        respuesta = (b"SIP/2.0 401 Unauthorized\r\n\r\n")
                        self.wfile.write(respuesta)
                        log("Sent to " + ip + ":" + str(puerto)
                            + ": " + str(respuesta), LOGS)

                else:
                    expira = int(mensaje.split("\r\n")[1].split(":")[1])
                    if expira != 0:
                        self.nonce[usuario] = secrets.token_hex(12)
                        respuesta = "SIP/2.0 401 Unauthorized\r\n" \
                                    + 'WWW Authenticate: Digest nonce="' \
                                    + self.nonce[usuario] + '"\r\n\r\n'
                        self.wfile.write(bytes(respuesta, 'utf-8'))
                        log("Sent to " + ip + ":" + str(puerto)
                            + ": " + respuesta, LOGS)
                    else:
                        self.wfile.write(b'SIP/2.0 404 User Not Found\r\n')
                        log("Sent to " + ip + ":" + str(puerto)
                            + ": SIP/2.0 404 User not Found", LOGS)

        self.register2json()


if __name__ == "__main__":
    archivo = sys.argv[1]
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit("File Not Found")
    DICCIONARIO3 = cHandler.get_tags()

    NAME = DICCIONARIO3['server name']
    IP = DICCIONARIO3['server ip']
    PUERTO = DICCIONARIO3['server puerto']
    localpassword = DICCIONARIO3['database passwdpath']
    localdatabase = DICCIONARIO3['database path']
    LOGS = DICCIONARIO3['log path']

    # Listens at localhost ('') port 6001
# and calls the EchoHandler class to manage the request
    serv = socketserver.UDPServer((IP, int(PUERTO)), SIPRegisterHandler)
    print("Server Nuevo listening at port 6001...")
    log("Starting proxy...", LOGS)
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        log("Finishing...", LOGS)
