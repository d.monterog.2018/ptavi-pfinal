#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
import secrets
import random
from xml.sax import make_parser
from uaclient import SmallSMILHandler, log


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    rtpaudio = []

    def handle(self):
        """Echo handle class."""
        # Escribe dirección y puerto del cliente (de tupla client_address)

        mensaje = self.rfile.read().decode('utf-8')
        print(mensaje)
        if mensaje != '\r\n':
            metodo = mensaje.split(" ")[0]
            sip = mensaje.split(" ")[1].split(":")[0]
            log("Recived from: " + PROXYIP + ":" + PROXYPUERTO
                + ": " + mensaje, LOG)

            if sip != 'sip':
                respuesta = (b"SIP/2.0 400 Bad Request\r\n\r\n")
                self.wfile.write(respuesta)
                log("Send to: " + PROXYIP + ":" + PROXYPUERTO
                    + ": " + "SIP/2.0 400 Bad Request", LOG)
            elif metodo == 'INVITE':
                ip_rtp = mensaje.split("\r\n")[5].split(" ")[1]
                port_rtp = int(mensaje.split("\r\n")[8].split(" ")[1])
                print(mensaje.split("\r\n"))
                self.rtpaudio.append(ip_rtp)
                self.rtpaudio.append(port_rtp)
                respuesta = (b"SIP/2.0 100 Trying\r\n\r\n")
                respuesta2 = (respuesta + b"SIP/2.0 180 Ringing\r\n\r\n")
                respuesta3 = (respuesta2 + b"SIP/2.0 200 OK\r\n")
                respuesta5 = (b"v=0\r\no=" + bytes(usuario, 'utf-8')
                              + b" " + bytes(ip, 'utf-8')
                              + b"\r\ns=normal\r\nt=0\r\nm=audio "
                              + bytes(PUERTORTP, 'utf-8') + b" RTP\r\n")
                longitud = str(len(respuesta5))
                respuesta4 = (respuesta3
                              + b"Content-Type: application/sdp\r\n"
                                b"Content-Length: "
                              + bytes(longitud, 'utf-8') + b"\r\n\r\n"
                              + respuesta5)
                self.wfile.write(respuesta4)
                log("Send to: " + PROXYIP + ":" + PROXYPUERTO
                    + ": " + str(respuesta4), LOG)
            elif metodo == 'BYE':
                respuesta = (b"SIP/2.0 200 OK\r\n\r\n")
                self.wfile.write(respuesta)
                log("Send to: " + PROXYIP + ":" + PROXYPUERTO
                    + ": " + str(respuesta), LOG)

            elif metodo == 'ACK':

                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT,
                                      payload_type=14, ssrc=200002)
                csrc = [random.randint(1, 5), random.randint(1, 6),
                        random.randint(1, 7)]
                RTP_header.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(AUDIO)
                simplertp.send_rtp_packet(RTP_header, audio,
                                          self.rtpaudio[0], self.rtpaudio[1])
                self.rtpaudio = []

            elif metodo != ('INVITE', 'BYE', 'ACK'):
                respuesta = (b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
                self.wfile.write(respuesta)
                log("Send to: " + PROXYIP + ":"
                    + PROXYPUERTO + ": " + str(respuesta), LOG)


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        configuracion = sys.argv[1]
    except IndexError:
        sys.exit("Usage: python3 uaserver.py config")
    except ValueError:
        sys.exit("Usage: python3 uaserver.py config")

    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(configuracion))
    except FileNotFoundError:
        sys.exit("File Not Found")

    DICCIONARIO2 = cHandler.get_tags()

    if DICCIONARIO2['uaserver ip'] == " ":
        ip = "127.0.0.1"
    else:
        ip = DICCIONARIO2['uaserver ip']
    print("IP: " + ip)

    puerto = DICCIONARIO2['uaserver puerto']
    print("PUERTO: " + puerto)
    usuario = DICCIONARIO2['account username']
    print("USUARIO:" + usuario)
    PSSWORD = DICCIONARIO2['account passwd']
    PUERTORTP = DICCIONARIO2['rtpaudio puerto']
    print("PUERTO RTP: " + PUERTORTP)
    PROXYIP = DICCIONARIO2['regproxy ip']
    print("IP PROXY: " + PROXYIP)
    PROXYPUERTO = DICCIONARIO2['regproxy puerto']
    print("PUERTO PROXY: " + PROXYPUERTO)
    LOG = DICCIONARIO2['log']
    AUDIO = DICCIONARIO2['audio']

    serv = socketserver.UDPServer((ip, int(puerto)), EchoHandler)
    print("Lanzando servidor UDP de eco...")
    log("Starting...", LOG)
    print("Listening...")
    serv.serve_forever()
    log("Finishing...", LOG)
