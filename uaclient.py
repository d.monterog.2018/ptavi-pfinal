#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import socket
import sys
import hashlib
import simplertp
import secrets
import random
from datetime import datetime


class SmallSMILHandler(ContentHandler):
    """Content Handler."""

    def __init__(self):
        """Inicializamos."""
        self.lista = {}  # Creamos la lista
        self.etiquetaactual = ''
        self.valores = {'account': ['username', 'passwd'],
                        'uaserver': ['ip', 'puerto'],
                        'rtpaudio': ['puerto'],
                        'regproxy': ['ip', 'puerto'],
                        'log': [''],
                        'audio': ['']}

    def startElement(self, name, atributos):
        """Lee las etiquetas del xml con sus valores."""
        self.etiquetaactual = name
        if name in self.valores:
            for att in self.valores[name]:
                self.lista[name + ' ' + att] = atributos.get(att, '')

    def endElement(self, name):
        """End Element."""
        self.etiquetaactual = ''

    def characters(self, content):
        """Characters."""
        if self.etiquetaactual in ['log', 'audio']:
            self.lista[self.etiquetaactual] = content

    def get_tags(self):
        """Devuelve el diccionario con los atributos."""
        return self.lista  # Nos devuelve la lista


def log(mensaje, logs):
    """Creacion y escritura de logs."""
    fichero = open(logs, "a")
    formato = "%Y-%m-%d %H:%M:%S "
    fecha = datetime.now().strftime(formato)
    fichero.write(fecha)
    fichero.write(mensaje.replace("\r\n", " ") + "\n")
    fichero.close()


def response(nonce, pssword):
    """Funcion para crear un hash a partir del nonce."""
    digest = hashlib.sha256()
    digest.update(bytes(nonce + pssword, 'utf-8'))
    digest.digest()
    return digest.hexdigest()


if __name__ == "__main__":

    archivo = sys.argv[1]
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    try:
        parser.parse(open(archivo))
    except FileNotFoundError:
        sys.exit("File Not Found")
    DICCIONARIO = cHandler.get_tags()

    try:
        metodo = sys.argv[2].upper()
        mensaje = sys.argv[3]
    except ValueError:
        sys.exit("Usage: python3 uaclient.py config method option")
    except IndexError:
        sys.exit("Usage: python3 uaclient.py config method option")
    if DICCIONARIO['uaserver ip'] == " ":
        IP = "127.0.0.1"
    else:
        IP = DICCIONARIO['uaserver ip']
    print("IP: " + IP)

    PUERTO = DICCIONARIO['uaserver puerto']
    USUARIO = DICCIONARIO['account username']
    pssword = DICCIONARIO['account passwd']
    PUERTORTP = DICCIONARIO['rtpaudio puerto']
    PROXYIP = DICCIONARIO['regproxy ip']
    PROXYPUERTO = DICCIONARIO['regproxy puerto']
    LOG = DICCIONARIO['log']
    AUDIO = DICCIONARIO['audio']
    LINEA = ("v=0\r\n" + "o=" + USUARIO + " " + IP + "\r\n"
             + "s=SoyPedroElJefe\r\n"
             + "t=0\r\n" + "m=audio " + PUERTORTP + " RTP")
    longitud = len(LINEA)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((PROXYIP, int(PROXYPUERTO)))
        log("Starting...", LOG)

        if metodo == 'INVITE':
            LINE = (metodo + " sip:" + mensaje + " SIP/2.0" "\r\n"
                    + "Content-Type: application/sdp\r\n" + "Content-Length: "
                    + str(longitud) + "\r\n\r\n" + LINEA)
            log("Sent to " + IP + ":" + PUERTO + ": " + LINE, LOG)
        elif metodo == 'REGISTER':
            LINE = (metodo + " sip:" + USUARIO + ":" + PUERTO
                    + " SIP/2.0" "\r\n" + "Expires:" + str(mensaje) + "\r\n")
            log("Sent to " + IP + ":" + PUERTO + ": " + LINE, LOG)
        elif metodo == 'BYE':
            LINE = (metodo + " sip:" + mensaje + " SIP/2.0")
            log("Sent to " + IP + ":" + PUERTO + ": " + LINE, LOG)
        elif metodo != 'REGISTER' and metodo != 'INVITE' and metodo != 'BYE':
            LINE = (metodo + " sip:" + mensaje + " SIP/2.0")
            log("Sent to " + IP + ":" + PUERTO + ": " + LINE, LOG)

        print("Enviando datos...: " + LINE)
        my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)

        print('Recibido -- ', data.decode('utf-8'))
        mensajeser = data.decode('utf-8')
        log("Recived from: " + IP + ":" + PUERTO + ": " + mensajeser, LOG)
        if 'SIP/2.0 400 Bad Request' in mensajeser:
            sys.exit("Recibido: Peticion mal realizada")

        if metodo == 'INVITE':
            respuestaser = data.decode('utf-8')
            if 'SIP/2.0 404 User Not Found' in respuestaser:
                sys.exit("Recibido: Usuario no encontrado, necesita registro")
            respuestaser2 = respuestaser.split("\r\n")[4]
            if respuestaser2 == 'SIP/2.0 200 OK':
                print(mensajeser.split("\r\n"))
                iprtp = mensajeser.split("\r\n")[9].split(" ")[1]
                puertortp = int(mensajeser.split("\r\n")[12].split(" ")[1])
                LINE2 = ('ACK' + ' sip:' + mensaje + " SIP/2.0")
                log("Sent to " + IP + ":" + PUERTO + ": " + LINE2, LOG)
                print("Enviando ACK...: " + LINE2)
                my_socket.send(bytes(LINE2, 'utf-8') + b'\r\n')
                BIT = secrets.randbelow(1)
                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(version=2, marker=BIT,
                                      payload_type=14, ssrc=400004)
                csrc = [random.randint(1, 5), random.randint(1, 6),
                        random.randint(1, 7)]
                RTP_header.setCSRC(csrc)
                audio = simplertp.RtpPayloadMp3(AUDIO)
                simplertp.send_rtp_packet(RTP_header, audio, iprtp, puertortp)

        if metodo == 'REGISTER':
            respuestaservidor = mensajeser.split("\r\n")[0]
            if respuestaservidor == 'SIP/2.0 401 Unauthorized':
                nonce = mensajeser.split("\r\n")[1].split("=")[1]\
                    .split("\"")[1]
                linea2 = (metodo + " sip:" + USUARIO + ":" + PUERTO +
                          " SIP/2.0" "\r\n" + "Expires:"
                          + str(mensaje) + "\r\n")
                lineanueva = linea2 + 'Authorization: Digest response="' \
                                    + response(nonce, pssword) + '"'
                my_socket.send(bytes(lineanueva, 'utf-8') + b'\r\n\r\n')
                print("Enviando....", lineanueva)
                log("Sent to " + IP + ":" + PUERTO + ": " + lineanueva, LOG)
                data = my_socket.recv(1024)
                log("Recived from: " + IP + ":" + PUERTO + ": "
                    + str(data), LOG)
                print('Recibido -- ', data.decode('utf-8'))

        print("Terminando socket...")
        print("Fin.")
        log("Finishing.", LOG)
